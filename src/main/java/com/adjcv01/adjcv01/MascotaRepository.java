package com.adjcv01.adjcv01;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
public interface MascotaRepository extends CrudRepository<Mascota, Integer> {
    List<Mascota> findByEspecie(String especie);
    List<Mascota> findByPropietario(Propietario propietario);


    @Query("SELECT DISTINCT especie FROM Mascota")
    public List<String> findDistinctMascotaFromDb();
}
