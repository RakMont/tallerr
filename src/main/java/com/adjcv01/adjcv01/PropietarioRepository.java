package com.adjcv01.adjcv01;

import org.springframework.data.repository.CrudRepository;

public interface PropietarioRepository extends CrudRepository<Propietario, Integer> {
}
